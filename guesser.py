import logging
import os
import time
from multiprocessing import Process
from typing import List, Union
import random
import sys
import filetype
from playsound import playsound
import numpy as np


class GameSong:

    def __init__(self, name: str, path: os.PathLike, number: int = None):
        self.name: str = name  # file name of song
        self.path: os.PathLike = path  # path to audio file
        self.number: int = number  # a number assigned when the song is choosen for a game, so it can be selected by it

    def __repr__(self):
        if self.number:
            return f"Song number {self.number}: '{self.name}' at '{self.path}'"
        else:
            return f"Song: '{self.name}' at '{self.path}'"


class Game:

    def __init__(self, songs_path: os.PathLike):
        self.songs: List[GameSong] = self._init_songs(songs_path)  # List of all available game songs

        self.gamesongs: Union[List[GameSong], None] = None  # List of songs available in this round
        self.songtoguess: Union[GameSong, None] = None  # song to guess this round

        self.music_process: Union[Process, None] = None  # process playing the song (stored here, so it can be stopped)

        self.round: int = 0  # how many songs were already played (for calculating avg guessing time)
        self.starttime: float = time.time()  # time when the last round began
        self.last_score: float = 0
        self.total_score: float = 0
        self.last_guess_duration: float = 0  # seconds it took to guess the last song
        self.avg_guess_duration: float = 0  # average seconds to guess a song over all rounds

    def new_round(self, number_of_songs: int = 5):
        """
        Choose number_of_songs new songs to guess from, start playing one of them and start counting the time.

        :param number_of_songs: Number of songs presented as choices to the player
        """
        self._choose_new_songs(number=number_of_songs)

        self.round += 1

        # start the song as a separate process, so it runs asynchronously and can be killed (other than e.g. a thread)
        self.music_process = Process(target=play_song, args=(self.songtoguess,))
        self.music_process.start()

        self.starttime = time.time()

    def update_score(self):
        """
        Count the seconds since the round started and calculate a score based on it.
        """
        now: float = time.time()
        self.last_guess_duration = now - self.starttime

        score = -29*np.log(self.last_guess_duration)+100  # magic formula, 100 points after 1 second, approx. 0 at 30s

        if score < 0:  # scores get negative between 31 and 32, but I don't want negative scores, so just 0 then
            score = 0

        self.last_score = round(score, 2)
        self.total_score = round(self.total_score + score, 2)

        self.avg_guess_duration = (self.avg_guess_duration * (self.round - 1) + self.last_guess_duration) / self.round

    @staticmethod
    def _init_songs(musicdir: os.PathLike) -> List[GameSong]:
        """
        Scan all available songs for playing

        :param musicdir: Directory with songs
        :return: List of song paths
        """

        files: List[os.DirEntry] = scan_dir_recursive(musicdir)

        if not files:
            logging.critical(f"Keine Songs gefunden! Kopiere welche nach {musicdir} oder wähle einen anderen Ordner.")
            sys.exit(1)

        songs: List[GameSong] = []

        for file in files:
            ftype = filetype.guess_mime(file.path)
            if ftype is None:
                logging.debug(f"Skipping file of unknown MIME type {file.path}")
                continue
            elif ftype.startswith('audio/'):
                logging.debug(f"Adding file {file.path} of type {ftype}")
                songs.append(GameSong(path=file.path, name=file.name))
            else:
                logging.debug(f"Skipping file {file.path} of type {ftype}")

        logging.debug(f"Songs: {songs}")

        return songs

    def _choose_new_songs(self, number: int = 5) -> None:
        """
        Randomly select $number songs from all songs for this round and choose one song the player has to guess from it

        :param number: Number of Songs to select
        """

        # very lazy hack for the CLI input handling
        if number > 9:
            logging.error("Currently a maximum of nine songs is supported to guess from. Choosing 9 instead.")
            number = 9

        game_songs: List[GameSong] = random.sample(self.songs, number)
        logging.debug(f"Song choices of this round are {game_songs}")

        song_to_guess: GameSong = random.sample(game_songs, 1)[0]
        logging.debug(f"The song to be guessed will be {song_to_guess}")

        for i in range(len(game_songs)):
            game_songs[i].number = i + 1

        logging.debug(game_songs)

        self.gamesongs = game_songs
        self.songtoguess = song_to_guess


def scan_dir_recursive(d: os.PathLike, f: Union[List[os.DirEntry], None] = None) -> Union[List[os.DirEntry], None]:
    """
    Scan directory d recursively and collect all found files in f.

    :param d: root dir to scan
    :param f: list of found files
    :return: f
    """

    if f is None:
        f = []

    s = os.scandir(d)

    for entry in s:
        if entry.is_file():
            f.append(entry)
        if entry.is_dir():
            scan_dir_recursive(entry, f)

    return f


def play_song(song: GameSong) -> None:
    """
    Play the given audio file asynchronously.

    :param song: GameSong object
    """

    playsound(str(song.path), True)
