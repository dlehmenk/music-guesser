#!/usr/bin/env python3

import argparse
import signal
import sys
import readchar
from readchar import key

import guesser


# Catch Ctrl+C
def sigint_handler(sig, frame):
    print('Ok, bye.')
    sys.exit(0)


signal.signal(signal.SIGINT, sigint_handler)


def parse_args() -> argparse.Namespace:
    """
    parse CLI arguments

    :return: argparse Namespace with arguments
    """

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--music-dir", '-d', help="Path to directory with songs", type=str, required=True)

    return parser.parse_args()


def main():

    # import logging
    # logging.basicConfig(level=logging.DEBUG)

    args = parse_args()

    # create a new game with given song directory
    game = guesser.Game(songs_path=args.music_dir)

    print("Hallo! Kannst du raten, welcher Song läuft?\n"
          "Zum Loslegen drück irgendeine Taste, mit 'q' kannst du das Spiel danach jederzeit beenden.")
    input()

    # start first game round
    game.new_round()

    # main loop
    while True:

        # print all song numbers and file names, without trailing file extension
        for song in game.gamesongs:
            print(f'{song.number}) {".".join(song.name.split(sep=".")[0:-1])}')

        print("\nWelcher isses?\n")
        # read a single character from stdin
        answ = readchar.readchar()

        # if the number of the correct song is entered
        if answ == str(game.songtoguess.number):

            # stop the time and update the score
            game.update_score()
            # (the music is still playing until an option is choosen further down)
            print(f'Jawollo! 🎉\nDu hast "{".".join(game.songtoguess.name.split(sep=".")[0:-1])}" '
                  f'nach {round(game.last_guess_duration, 2)} Sekunden erraten! Das gibt {game.last_score} Punkte!\n\n'
                  f'Deine Gesamtpunktzahl nach {game.round} Runden ist: {game.total_score}.\n'
                  f'Im Durchschnitt hast du pro Song {round(game.avg_guess_duration, 2)} Sekunden gebraucht.\n\n'
                  f'Nochmal ([J]a/[n]ein)?\n')

            # maxmimum of 5 trys, otherwise we quit, because the user might be confused and randomly smashing buttons
            for i in range(5):
                answ = readchar.readchar()
                if answ.lower() == 'j' or answ.lower() == key.ENTER:
                    # stop the song and start the next round
                    game.music_process.kill()
                    game.new_round()
                    break
                elif answ.lower() == 'n' or answ.lower() == 'q':
                    # stop the song and quit
                    print('Bis zum nächsten mal!')
                    game.music_process.kill()
                    sys.exit(0)
                elif i >= 4:
                    # stop the song and quit
                    print('Ich versteh nur Bahnhof und mach erst mal Feierabend ☕')
                    game.music_process.kill()
                    sys.exit(0)
                else:
                    # try again
                    print('Hm? Willst du noch eine Runde spielen, [J]a oder [N]ein? (J oder N drücken)')

        elif answ == 'q':
            # stop the song and quit
            game.music_process.kill()
            print('Tschüüüss')
            sys.exit(0)

        else:
            # try again
            print(f"Nee, nich {answ}, versuch's nochmal!\n")


if __name__ == "__main__":
    main()
